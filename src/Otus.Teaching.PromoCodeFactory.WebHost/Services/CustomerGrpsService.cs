﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Protos;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerGrpsService: CustomerGrpc.CustomerGrpcBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerGrpsService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async override Task<CustomerGrpsResponse> CreateCustomerAsync(CreateOrEditCustomerGrpsRequest request, ServerCallContext context)
        {
            var preferencesIds = request.PreferenceIds.Select(x => Guid.Parse(x.Value)).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);

            Customer customer =  CustomerMapper.MapFromGprsModel(request, preferences);

            await _customerRepository.AddAsync(customer);

           return new CustomerGrpsResponse()
            {
                Id = new Id { Value = customer.Id.ToString() },
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };
        }

        public async override Task<Empty> DeleteCustomerAsync(Id request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Value));

            if (customer == null)
            {
                var metadata = new Metadata
                    {
                        { "customerId: ", customer.Id.ToString() }
                    };

                throw new RpcException(new Status(StatusCode.NotFound, "Permission denied"), metadata);
            }

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }

        public async override Task<Empty> EditCustomersAsync(CreateOrEditCustomerGrpsRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id.Value));

            if (customer == null)
            {
                var metadata = new Metadata
                    {
                        { "customerId: ", customer.Id.ToString() }
                    };

                throw new RpcException(new Status(StatusCode.NotFound, "Permission denied"), metadata);
            }

            var preferencesIds = request.PreferenceIds.Select(x => Guid.Parse(x.Value)).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);


            CustomerMapper.MapFromGprsModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public async override Task<CustomerGrpsResponse> GetCustomerAsync(Id request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Value));

            var response = new CustomerGrpsResponse()
            {
                Id = new Id { Value = customer.Id.ToString() },
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };

            if (customer.Preferences.Count <= 0)
            {
                return response;

            }
            foreach (var preference in customer.Preferences)
            {
                response.Preferences.Add(new PreferenceGrpsResponse
                {
                    Id = new Id { Value = preference.PreferenceId.ToString() },
                    Name = preference.Preference.Name
                });
            }

            return response;
        }

        public async override Task GetCustomersAsync(Empty request, IServerStreamWriter<CustomerGrpsShortResponse> responseStream, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersArray =  customers.Select(x => new CustomerGrpsShortResponse()
            {
                Id = new Id { Value = x.Id.ToString() },
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            foreach ( var customer in customersArray)
            {
                await responseStream.WriteAsync(customer);
            }
        }
    }
}
